This is a WIP attempt to simplify p2p connections over wifi-direct and showcase 
some of the interesting use cases.


To date it handles:


-Broadcasting of services

-Discovery of broadcast services (and forwarding to other services via intent)

-Toggling of P2P broadcast (without user interaction)

-A Simple proxy server that allow clients to connect to host wifi (modified from built in AOSP version)

